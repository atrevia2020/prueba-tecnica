<?php


namespace App\Infrastructure\Service;


use App\Domain\Model\DomainEvent;
use Symfony\Component\Messenger\MessageBus;

class EventDispatcher extends MessageBus implements \App\Application\Service\EventDispatcher
{
    public function notify(DomainEvent ...$events): void
    {
        foreach ($events as $event){
            $this->dispatch($event);
        }
    }
}