<?php


namespace App\Infrastructure\Listener;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ExceptionListener
{
    public const ENTITY_NOT_FOUND = 'Entity not found';

    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();
        $formattedError = $exception->getMessage();

        if ($exception instanceof NotFoundHttpException && strpos($exception->getMessage(), 'App\Domain') === 0) {
            $formattedError = self::ENTITY_NOT_FOUND;
        }

        $response = new JsonResponse(['message' => $formattedError]);
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);

        if ($exception instanceof HttpExceptionInterface) {
            $response->setStatusCode($exception->getStatusCode());
            $response->headers->replace($exception->getHeaders());
        } else {
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $event->setResponse($response);
    }
}