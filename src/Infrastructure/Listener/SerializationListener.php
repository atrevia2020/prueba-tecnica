<?php


namespace App\Infrastructure\Listener;


use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;

class SerializationListener
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function onKernelView(ViewEvent $viewEvent)
    {
        $value = $viewEvent->getControllerResult();

        if ($value === null){
            return;
        }

        $context = SerializationContext::create();
        $context->enableMaxDepthChecks();
        $response = new Response($this->serializer->serialize($value, "json", $context));
        $response->headers->set('Content-Type', 'application/json');
        $viewEvent->setResponse($response);

    }
}