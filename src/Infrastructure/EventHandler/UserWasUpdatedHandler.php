<?php


namespace App\Infrastructure\EventHandler;


use App\Domain\Event\UserWasUpdated;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class UserWasUpdatedHandler implements MessageHandlerInterface
{
    public function __invoke(UserWasUpdated $userWasUpdated){
        // Do something...
        // Send some mails...
    }
}