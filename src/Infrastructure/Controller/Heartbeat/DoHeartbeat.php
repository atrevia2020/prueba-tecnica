<?php


namespace App\Infrastructure\Controller\Heartbeat;

use App\Infrastructure\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DoHeartbeat extends BaseController
{
    /**
     * @Route("/", methods={"GET"})
     * @param Request $request
     * @return array
     */
    public function __invoke(Request $request)
    {
        return ['OK'];
    }
}