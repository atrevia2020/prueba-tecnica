<?php


namespace App\Infrastructure\Controller\User;

use App\Application\UseCase\DoUpdateUser\DoUpdateUserCommand;
use App\Application\UseCase\DoUpdateUser\QueryUserByIdCommandHandler;
use App\Application\UseCase\QueryUserById\QueryUserByIdCommand;
use App\Domain\Model\User\Event\UserWasUpdated;
use App\Infrastructure\Controller\BaseController;
use App\Infrastructure\Decorator\TransactionDecorator;
use App\Infrastructure\Repository\UserRepository;
use App\Infrastructure\Service\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBus;
use Symfony\Component\Routing\Annotation\Route;

class DoUpdateUser extends BaseController
{
    /**
     * @Route("/user/{id}", name="do_update_user", methods={"PUT","HEAD"})
     * @param Request $request
     * @param $id
     * @return array
     */
    public function __invoke(Request $request, $id)
    {
        $managerRegistry = $this->getDoctrine();
        $objectManager = $managerRegistry->getManager();

        $command = new QueryUserByIdCommand(
            $id
        );

        $commandHandler = new QueryUserByIdCommandHandler(
            new UserRepository($managerRegistry)
        );

        $transaction = new TransactionDecorator(
            $commandHandler,
            $objectManager
        );

        return $transaction->execute($command);
    }
}