<?php


namespace App\Infrastructure\Controller;


use Doctrine\Persistence\ObjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\Exception\ParameterNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

abstract class BaseController extends AbstractController
{
    public function repo($class): ObjectRepository
    {
        return $this->getDoctrine()->getRepository($class);
    }

    protected function param($param)
    {
        /* @var Request $request */
        $request = $this->container->get('request_stack')->getMasterRequest();

        return $request->request->get($param);
    }

    protected function query($param)
    {
        /* @var Request $request */
        $request = $this->container->get('request_stack')->getMasterRequest();

        return $request->query->get($param);
    }

    protected function header($param)
    {
        /* @var Request $request */
        $request = $this->container->get('request_stack')->getMasterRequest();
        return $request->headers->get($param);
    }
}