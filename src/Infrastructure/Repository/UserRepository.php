<?php

namespace App\Infrastructure\Repository;

use App\Domain\Model\User\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\Persistence\ManagerRegistry;

class UserRepository extends ServiceEntityRepository implements \App\Application\Repository\UserRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function findByIdOrFail($id): User
    {
        /** @var User $user */
        $user = $this->find($id);

        if ($user === null){
            throw new EntityNotFoundException('User does not exists');
        }

        return $user;
    }

    public function save(User $user): void
    {
        $this->getEntityManager()->persist($user);
    }

}