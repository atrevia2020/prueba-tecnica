<?php


namespace App\Infrastructure\Decorator;


use App\Application\UseCase\CommandHandler;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectManager;

class TransactionDecorator
{
    private $commandHandler;
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(CommandHandler $commandHandler, ObjectManager $entityManager)
    {

        $this->commandHandler = $commandHandler;
        $this->entityManager = $entityManager;
    }

    public function execute($command){

        $response = $this->commandHandler->execute($command);

        $this->entityManager->flush();

        return $response;
    }

}