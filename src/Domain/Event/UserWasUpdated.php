<?php


namespace App\Domain\Event;

use App\Domain\Model\DomainEvent;

class UserWasUpdated implements DomainEvent
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var \DateTime
     */
    private $occurredOn;

    public function __construct(
        int $id,
        string $name
    )
    {
        $this->occurredOn = new \DateTime();
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return \DateTime
     */
    public function getOccurredOn(): \DateTime
    {
        return $this->occurredOn;
    }

    /**
     * @param \DateTime $occurredOn
     */
    public function setOccurredOn(\DateTime $occurredOn): void
    {
        $this->occurredOn = $occurredOn;
    }

}