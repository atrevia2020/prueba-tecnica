<?php

namespace App\Domain\Model\ValueObject;

interface StringValueObject extends ValueObject
{
    public function __construct(string $value);
}