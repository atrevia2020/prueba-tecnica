<?php

namespace App\Domain\Model\ValueObject;

interface FloatValueObject extends ValueObject
{
    public function __construct(float $value);
}