<?php

namespace App\Domain\Model\ValueObject;

interface BooleanValueObject extends ValueObject
{
    public function __construct(bool $value);
}