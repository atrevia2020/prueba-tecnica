<?php

namespace App\Domain\Model\ValueObject;

interface ValueObject
{
    public function value();
}