<?php


namespace App\Domain\Model\User;

use App\Domain\Event\UserWasUpdated;
use App\Domain\Model\AggregateRoot;
use App\Domain\Model\User\ValueObject\UserNameValueObject;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Infrastructure\Repository\UserRepository")
 */
class User extends AggregateRoot
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @return mixed
     */
    private function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    private function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    private function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    private function setName($name): void
    {
        $this->name = $name;
    }

    public function update(UserNameValueObject $nameValueObject): void
    {
        $this->setName($nameValueObject->value());

        $this->record(new UserWasUpdated($this->getId(), $this->getName()));
    }



}