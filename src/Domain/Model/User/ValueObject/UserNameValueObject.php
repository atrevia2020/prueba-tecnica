<?php


namespace App\Domain\Model\User\ValueObject;

use App\Domain\Model\ValueObject\StringValueObject;

class UserNameValueObject implements StringValueObject
{

    private $value;

    public function __construct(string $value)
    {
        $this->ensureMinLength($value);
        $this->value = $value;
    }

    public function value()
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @throws \Exception
     */
    private function ensureMinLength(string $value): void
    {
        if (strlen($value) < 3) {
            throw new \InvalidArgumentException('Name must be longer');
        }
    }

}