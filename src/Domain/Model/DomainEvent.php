<?php

namespace App\Domain\Model;

interface DomainEvent
{
    /**
     * @return \DateTime
     */
    public function getOccurredOn(): \DateTime;

    /**
     * @param \DateTime $occurredOn
     */
    public function setOccurredOn(\DateTime $occurredOn): void;
}