<?php


namespace App\Domain\Model;


class AggregateRoot
{
    private $events = [];

    /**
     * @return array
     */
    public function pullEvents(): array
    {
        $events = $this->events;
        $this->events = [];

        return $events;
    }

    /**
     * @param $event
     */
    public function record($event): void
    {
        $this->events[] = $event;
    }

}