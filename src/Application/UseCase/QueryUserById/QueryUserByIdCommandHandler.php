<?php


namespace App\Application\UseCase\DoUpdateUser;

use App\Application\UseCase\CommandHandler;
use App\Application\Repository\UserRepository;
use App\Application\Service\EventDispatcher;
use App\Domain\Model\User\User;
use App\Domain\Model\User\ValueObject\UserNameValueObject;

class QueryUserByIdCommandHandler implements CommandHandler
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var EventDispatcher
     */

    public function __construct(
        UserRepository $userRepository
    )
    {
        $this->userRepository = $userRepository;
    }

    public function execute(DoUpdateUserCommand $command): User
    {
        return  $this->userRepository->findByIdOrFail($command->getId());
    }
}