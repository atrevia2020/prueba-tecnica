<?php

namespace App\Application\UseCase\QueryUserById;

use App\Application\UseCase\Command;

class QueryUserByIdCommand implements Command
{

    /**
     * @var float
     */
    private $id;


    public function __construct(
        float $id
    )
    {
        $this->id = $id;
    }

    /**
     * @return float
     */
    public function getId(): float
    {
        return $this->id;
    }

    /**
     * @param float $id
     */
    public function setId(float $id): void
    {
        $this->id = $id;
    }

}