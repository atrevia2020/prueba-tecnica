<?php


namespace App\Application\UseCase\DoUpdateUser;

use App\Application\UseCase\CommandHandler;
use App\Application\Repository\UserRepository;
use App\Application\Service\EventDispatcher;
use App\Domain\Model\User\User;
use App\Domain\Model\User\ValueObject\UserNameValueObject;

class DoUpdateUserCommandHandler implements CommandHandler
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var EventDispatcher
     */
    private $eventDispatcher;

    /**
     * @param UserRepository $userRepository
     * @param EventDispatcher $eventDispatcher
     */

    public function __construct(
        UserRepository $userRepository,
        EventDispatcher $eventDispatcher
    )
    {
        $this->userRepository = $userRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function execute(DoUpdateUserCommand $command): User
    {
        $user = $this->userRepository->findByIdOrFail($command->getId());

        $user->update(new UserNameValueObject($command->getName()));

        $this->userRepository->save($user);

        $this->eventDispatcher->notify($user->pullEvents());

        return $user;
    }
}