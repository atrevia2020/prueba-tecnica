<?php

namespace App\Application\UseCase\DoUpdateUser;

use App\Application\UseCase\Command;

class DoUpdateUserCommand implements Command
{

    /**
     * @var float
     */
    private $id;
    /**
     * @var string
     */
    private $name;

    public function __construct(
        float $id,
        string $name
    )
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @return float
     */
    public function getId(): float
    {
        return $this->id;
    }

    /**
     * @param float $id
     */
    public function setId(float $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }


}