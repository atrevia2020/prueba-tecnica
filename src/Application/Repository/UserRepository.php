<?php

namespace App\Application\Repository;

use App\Domain\Model\User\User;

interface UserRepository
{
    public function findByIdOrFail($id): User;

    public function save(User $user): void;
}